import axios from "axios";

const getData = async (user_id) =>{
    const {data:GetVeri} = await axios("https://jsonplaceholder.typicode.com/users/"+user_id);
    const {data:Postveri} = await axios("https://jsonplaceholder.typicode.com/posts?userId="+user_id);
    const mergeJson={
        GetVeri,
        Postveri
    }
    return mergeJson;
}
export default getData;